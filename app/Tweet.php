<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Log;
use TwitterAPIExchange;
use App\Plugins\Geocode\Goocode;
use App\Plugins\TwitterPlugin\TwitterPlugin;


class Tweet extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'text', 'lat', 'lng','user_profile','history_id'
    ];

    public function search($city)
    {
        $resp['status'] = 'error';
        $resp['message'] = 'No Tweets available';
        //$resp['data'] = [];
dd($this->get_map());
        try{
            if(!$city){
                $resp['message'] = 'Invalid City';
                return $resp;
            }
            DB::beginTransaction();

            $history = new History;
            $history->title = $city;

            $insertHis = $history->save();



            if($insertHis ){
                DB::commit();
            }


        }catch (\Exception $e){
            $resp['message'] = $e->getMessage();
            DB::rollBack();
        }

        return $resp;
    }

    public function get_map()
    {
        //Receiving Post Data
        $address = urlencode($this->input->post('address'));
        $tweets_num = $this->input->post('tweets_num');
        $radius = $this->input->post('radius');
        if($radius == '' || $radius == 0){
            $radius = 50;
        }
        if($tweets_num == '' || $tweets_num == 0 || $tweets_num > 100){
            $tweets_num = 15;
        }
        //Using google API to get city coordinates
        $url = 'http://maps.google.com/maps/api/geocode/json?address='.$address;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geoloc = json_decode(curl_exec($ch), true);
        if(count($geoloc['results']) != 0){
            $formatted_address = $geoloc['results'][0]['formatted_address'];
            $lat = $geoloc['results'][0]['geometry']['location']['lat'];
            $lng = $geoloc['results'][0]['geometry']['location']['lng'];
            $location = $lat . ',' . $lng . ',' . $radius . 'km';
            //Using twitter API to search tweets by coordinates
            $settings = array(
                'oauth_access_token' => "631982613-KrFPScU3Z203jQ63pg5Sx4g277mSgfl0N1v6EBU7",
                'oauth_access_token_secret' => "Q7MGwYwnuWT4LQFr5iXCLZyd7PZYFQwW6BvQzxsRrWzjQ",
                'consumer_key' => "HYLrNMAxj2L5DenPyjvWJPDbL",
                'consumer_secret' => "UT3J1ONFc1aWQAEBFHYfvjYCynWhFCAqdeMEDuZvWaSOvNMyGo"
            );
            $searchURL = 'https://api.twitter.com/1.1/search/tweets.json';
            $requestMethod = 'GET';
            $getfield = '?lang=en&q=&count='.$tweets_num.'&geocode='.$location;
            $twitter = new TwitterAPIExchange($settings);
            $tweets = json_decode($twitter->setGetfield($getfield)
                ->buildOauth($searchURL, $requestMethod)
                ->performRequest());
            //Response tweets
            $items = $tweets->statuses;
            $tweets_array = array();
            foreach ($items as $key) {
                if($key->geo != null && count($key->geo) != 0 ){
                    $tweet_array = array(
                        'created'=>$key->created_at,
                        'tweet'=>$key->text,
                        'profile'=>$key->user->profile_image_url,
                        'username'=>$key->user->name,
                        'tag'=>$key->user->screen_name,
                        'geo'=>$key->geo
                    );
                    array_push($tweets_array, $tweet_array);
                }
            }
        }else{
            //No tweets
            $tweets_array = array();
            $formatted_address = '';
        }
        $result=array('tweets'=>$tweets_array,'status'=>'ok','formatted_address'=>$formatted_address);
        echo json_encode($result);
    }


}
