<?php

namespace App\Helpers;

/**
 * A VO class to store the twitter user information
 * Class TwitterUser.
 */
class TwitterUser
{
    /**
     * @var string Name of the Twitter user
     */
    public $name;

    /**
     * @var string Link to the twitter user profile image.
     */
    public $profile_picture;
}
