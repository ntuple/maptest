<?php

namespace App\Helpers;

class Tweets
{
    /**
     * @var array Status Collection
     */
    protected static $statusCollections;

    /**
     * @var array raw Status
     */
    protected static $rawStatuses;

    /**
     * Sets the statuses.
     *
     * @param array $statuses
     */
    public function __construct(array $statuses)
    {
        self::$rawStatuses = $statuses;
        self::$statusCollections = [];
    }

    /**
     * Returns the list of tweets that contained the searched location
     * somewhere in the tweet.
     *
     * @return array A point object containing latitude and longitude
     */
    public static function getByCoordinates()
    {
        foreach (self::$rawStatuses as $status) {

            // Get the lat, lng of the location
            $point = $status->coordinates->coordinates;

            if (!empty($point)) {
                // \Kint::dump($point);

                $st = new TwitterStatus();
                $st->tweet_text = $status->text;

                $d = new \DateTime($status->created_at);
                $st->created_at = $d->format('Y-m-d H:i:s');

                // The order is reverse for the data returned by Google GeoCoding
                $st->location = ['lat' => $point[1] ,'lng' => $point[0]];

                $st->user = new TwitterUser();
                $st->user->profile_picture = $status->user->profile_image_url;
                $st->user->name = $status->user->screen_name;

                self::$statusCollections[] = $st;
            }
        }

        return self::$statusCollections;
    }
}
