<?php
namespace App\Helpers\Contracts;

Interface TweetSearchContract {
    public function request($searchQuery);
}