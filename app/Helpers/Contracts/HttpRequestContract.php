<?php

namespace App\Helpers\Contracts;


interface HttpRequestContract
{
    public function setOption($name, $value);
    public function execute();
    public function getInfo($name);
    public function close();
}