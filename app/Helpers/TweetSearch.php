<?php
namespace App\Helpers;

use App\Helpers\Contracts\TweetSearchContract;

class TweetSearch implements TweetSearchContract
{
    /**
     * Configuration array.
     *
     * @var array
     */
    protected $config;

    /**
     * Response Object. After connecting to Twitter API it sends back json data
     * this json data is stored in $response object.
     *
     * @var Object
     */
    protected $response;

    /**
     * Base URI. Base url for the Twitter API.
     *
     * @var string Base URI
     */
    protected $base_uri;

    /**
     * @var string base64 encoded string Access Token
     */
    protected $access_token;

    /**
     * @var string In two-legged OAuth interaction this payload is used in
     *             second phase to retrieve the Bearer Token from the Twitter OAuth Connection
     */
    protected $payload;

    /**
     * @var array When successfull connection is made to Twitter API then
     *            this will hold the result. This also holds the error.
     */
    protected $search_results = null;

    /**
     * @var bool When enabled all the Twitter API calls will be logged to the
     *           file in log/twitter_plugin
     */
    protected $debug = false;

    /**
     * @var Logger|null App\Plugins\AppLogger instance to log the Twitter API
     *                  calls
     */
    protected $log = null;

    const CURL_TIMEOUT = 300;
    const SSLVERIFY_HOST = 2;
    const TOKEN_ENDPOINT = 'https://api.twitter.com/oauth2/token';
    const SEARCH_ENDPOINT = 'https://api.twitter.com/1.1/search/tweets.json';

    /**
     * TwitterPlugin constructor.
     *
     * @param array $config Configuration Parameters
     */
    public function __construct()
    {


    }

    /**
     * Wrapper function for addInfo to log the message.
     *
     * @param $message string Message that needs to be logged.
     */
    private function logInfo($message)
    {
//        print $message . PHP_EOL;
    }

    /**
     * Update the API configuration with the supplied configuration parameters.
     * The configuration is an array.
     *
     * @param $config array Configuration Parameters
     */
    public function setConfig($config)
    {
        $this->config = $config;

        $this->logInfo('Configuration updated from setConfig.');
    }

    /**
     * Encodes the consumer key and consumer secret and returns to the callee
     * function. The encoding is simple and is the concatenation of the
     * consumer_key:consumer_secret format.
     *
     * @return string encoded consumer key and consumer secret key.
     *
     * @see getBearerToken()
     */
    private function getEncodedConsumerKeyAndSecret()
    {
        $encode = base64_encode(
            sprintf('%s:%s', $this->config['consumer_key'], $this->config['consumer_secret'])
        );

        return $encode;
    }

    /**
     * Formats the bearer token and then uses the same token to send further
     * request to the twitter end point.
     */
    private function getBearerToken()
    {
        /*
         * @var string Encode the keys as per the guideline provided in the twitter page.
         */
        $encodedKeys = $this->getEncodedConsumerKeyAndSecret();


        $this->logInfo("encodedKeys : $encodedKeys");

        /*
         * @var array request header that is sent while making curl request
         * to the Twitter API EndPoint.
         */
        $request_headers = [];
        $request_headers[] = "Authorization: Basic {$encodedKeys}";

        /*
         * grant_type must be set to client_credentials
         *
         * @see https://dev.twitter.com/oauth/application-only
         */
        $post_data = 'grant_type=client_credentials';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::TOKEN_ENDPOINT);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, self::SSLVERIFY_HOST);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($curl, CURLOPT_TIMEOUT, self::CURL_TIMEOUT);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        // TODO handle the curl error that can occur
        $response = curl_exec($curl);

        /*  if(!$response){
              throw new \Exception("No Records");
          }*/

        $this->payload = $payload = json_decode($response);
        curl_close($curl);

        if ($this->payload === NULL) {
            throw new \Exception("Unknown Message Response from Twitter Received.");
        }

        // Successfully retrieved the twitter token
        if (!isset($payload->errors)) {
            if ($payload->token_type === 'bearer') {
                $this->access_token = $payload->access_token;
                $this->logInfo('Access Token Received from the API Endpoint.');
            }
        }

        $this->logInfo('Finished making API call for bearer token.');
    }

    /**
     * Send internal curl request.
     *
     * @param $url string Twitter EndPoint url where request needs to be made
     * @param null $data extra parameter
     *
     * @return mixed|null Returns the data
     */
    private function _sendCurl($url, $data = null)
    {
        $this->logInfo('Making API request for search');

        // Prepare the bearer token
        $this->getBearerToken();

        // Encode the keys as per the guideline provided in the twitter page.
        $encodedAccessToken = $this->payload->access_token;

//        ddd($encodedAccessToken);

        // Send Authorization Header to Twitter API
        $request_headers = [];
        $request_headers[] = "Authorization: Bearer {$encodedAccessToken}";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, self::SSLVERIFY_HOST);
//        if (getenv('APP_MODE') === 'development') {
//            curl_setopt($curl, CURLOPT_CAINFO, ini_get('curl.cainfo'));
//        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($curl, CURLOPT_TIMEOUT, self::CURL_TIMEOUT);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);

        $response = json_decode($result);

        curl_close($curl);

        $this->logInfo('Finished making API call for Search');

        // Make sure that we don't have errors
        return ($this->search_results = !isset($response->errors) ? $response : null);
    }

    /**
     * Utility function to format the Query String.
     *
     * @param array $q Query String
     *
     * @return array Returns formatted array
     */
    private function formatQuery($locationToSearch)
    {
//        print $locationToSearch.PHP_EOL;
        $geocode = new Geocode();
        $latlng = $geocode->getLatLng($locationToSearch);

        if (null != $latlng) {
            return [
                'q' => urlencode($locationToSearch),
                'geocode' => sprintf('%s,%s,%s', $latlng->lat, $latlng->lng, $this->config['search_radius_within']),
                'lang' => 'en',
            ];
        }

        return [];
    }

    public function request($locationToSearch)
    {
        $this->_init_config();
        $query = $this->formatQuery($locationToSearch);
        $this->response = $this->_sendCurl(self::SEARCH_ENDPOINT . '?' . http_build_query($query));

        $tweets = $this->response->statuses;

        if(0 === count($tweets)){
            return [];
        }

        $coll = [];
        foreach($tweets as $status){
            if(isset($status->coordinates)) {
                $st = new TwitterStatus();
                $st->tweet_text = $status->text;

                $d = new \DateTime($status->created_at);
                $st->created_at = $d->format('Y-m-d H:i:s');

                // The order is reverse for the data returned by Google GeoCoding

                $st->user = new TwitterUser();
                $st->user->profile_picture = $status->user->profile_image_url;
                $st->user->name = $status->user->screen_name;

                $point = $status->coordinates->coordinates;
                $st->location = ['lat' => $point[1] ,'lng' => $point[0]];

                $coll[] = $st;
            }
        }
        return $coll;
    }

    private function _init_config()
    {
        $this->config = $config = config('tweet_search');
        $this->base_uri = isset($config['base_uri']) ? $config['base_uri'] : 'https://twitter.com/search';
        $this->debug = (boolean)$config['debug'];
    }

    private function toArray()
    {

    }
}