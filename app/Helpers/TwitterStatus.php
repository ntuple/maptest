<?php
/**
 * Twitter Status class contains the helper functions to create the twitter
 * status.
 *
 * @filesource src\App\Plugins\TwitterPlugin\TwitterStatus.php
 *
 * @version 1.0
 *
 * @package App\Plugins\TwitterPlugin
 */
namespace App\Helpers;

/**
 * A class to store the twitter information required to us to display in the
 * google map.
 */
class TwitterStatus
{
    /**
     * @var string Actual Tweet by the user
     */
    public $tweet_text;

    /**
     * @var string User to whom the current tweet belong to
     */
    public $user;
    /**
     * @var string date of the tweet
     */
    public $created_at;

    /**
     * @var string Name of place that returned the current tweet in search result.
     */
    public $searched_for;

    /**
     * Creates a new array with basic twitter attributes.
     *
     * @param $tweet_text string Text containing the twitter status
     * @param TwitterUser $user Twitter User Object
     * @param $created_at string Timestamp at which the tweet was created
     * @param $location string Text containing the latitude and longitude
     *
     * @return array An array that is compatible to display on the map
     */
    public static function create($tweet_text, TwitterUser $user, $created_at,
                           $location)
    {
        return array(
            'tweet_text' => $tweet_text,
            'user' => $user,
            'created_at' => $created_at,
            'location' => $location,
        );
    }
}
