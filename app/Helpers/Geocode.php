<?php
namespace App\Helpers;
//use App\Helpers\Contracts\HttpRequestContract;

/**
 * Geocode is used for interacting with Google coordinates.
 */
class Geocode
{
    /**
     * Endpoint for the Google MAP API V3.
     *
     * @var ENDPOINT End Point for the google api geocode
     */
    const ENDPOINT = 'https://maps.googleapis.com/maps/api/geocode/json';

    /**
     * Curl Object to do all the HTTP related calls to Twitter API
     *
     * @var CurlRequest CurlRequest Object
     */
    protected $curlRequest;

    const CURL_TIMEOUT = 300;
    const SSL_VERIFYHOST = 2;

    /**
     * Constructor for the Curl Request Object
     * @param CurlRequest $curlRequest
     */
    public function __construct()
    {

    }

    /**
     * Returns the json encoded list of tweets.
     *
     * @param $address string Address for which the latitutde, longitude
     * of the location is to be obtained
     *
     * @return $results|null mixed Results
     */
    public function getLatLng($address)
    {

        $config = config('geocode');

        $query = [
            'key' => $config['gmap_api_key'],
            'address' => urlencode($address),
        ];

        $url = self::ENDPOINT . '?' . http_build_query($query);
        $this->curlRequest = new CurlRequest($url);
//        $this->curlRequest->setOption(CURLOPT_URL, $url);
        $this->curlRequest->setOption(CURLOPT_SSL_VERIFYPEER, true);
        $this->curlRequest->setOption(CURLOPT_SSL_VERIFYHOST, self::SSL_VERIFYHOST);
//        $this->curlRequest->setOption(CURLOPT_CAINFO, ini_get('curl.cainfo'));
        $this->curlRequest->setOption(CURLOPT_TIMEOUT, self::CURL_TIMEOUT);
        $this->curlRequest->setOption(CURLOPT_RETURNTRANSFER, true);
        $response = $this->curlRequest->execute();

        $results = json_decode($response);

        $this->curlRequest->close();

        $output = ($results->status == 'OK') ? $results->results[0]->geometry->location : null;
        return $output;
    }
}