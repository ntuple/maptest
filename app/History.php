<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    public function tweets()
    {
        return $this->hasMany('App\Tweet', 'history_id', 'id');
    }


    public function hasCache($address)
    {
        // see if we have address with us or not
        $record = $this->where(['title'=>$address])->latest()->first();

        // if we do not have the row it means we don't have this record with
        // us, thus create a new record and send the response as false
        if(!$record) {
            return false;
        }

        // Compare the two DateTime Object
        $created = Carbon::parse($record->created_at);
        $now = Carbon::now();
        $diff = $created->diffInHours($now);

        /**
         * If our time for cache is still withing range then pass true as
         * this means our cache is still valid.
         */
        return ($diff < config('MAP_CACHE_TIME_HOURS'));

        // compare the last query time with the current one

        // if cacheTimeInHour has passed return false otherwise true


    }
}
