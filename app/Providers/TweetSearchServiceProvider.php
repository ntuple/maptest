<?php

namespace App\Providers;

use App\Helpers\TweetSearch;
use App\Helpers\TweetSearchContract;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class TweetSearchServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register bindings in the container
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\Contracts\TweetSearchContract', function () {
            return new TweetSearch(config('twitter_search'));
        });
    }

    public function provides()
    {
        return ['App\Helpers\Contracts\TweetSearchContract'];
    }
}
