<?php

namespace App\Http\Controllers\api;

use App\History;
use App\Http\Controllers\Controller;
use App\Helpers\Contracts\TweetSearchContract;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cache;

class TweetController extends Controller
{
    protected  $tw;
    public function __construct(TweetSearchContract $tw)
    {
        $this->tw = $tw;

    }

    public static function replace_whitespace($key){
        return strtolower(str_replace(" ","_",strtolower(trim($key))));
    }

    public function search(Request $request)
    {
        $tw = $this->tw;
        $keyword = $request->get('city');
        $key = self::replace_whitespace($request->get('city'));
        $config = config('tweet_search');
        $ttl_cache_minutes = $config['ttl_cache'];
        if(Cache::get($key)){
            $history = History::where(['title'=>$keyword])->first();
            if($history){
                $history->searched_at = Carbon::now();
                $history->save();
            }
        }
        $value = Cache::remember($key, $ttl_cache_minutes, function() use ($tw, $keyword){
            $history = History::where(['title'=>$keyword])->first();
            if(!$history){
                $history = new History;
            }
            $history->title = $keyword;
            $history->save();
            return $tw->request($keyword);
        });

        return response()->json(['data'=>$value]);
    }


    public function history()
    {
        $history = History::orderBy('searched_at','desc')->pluck('title');
        $history->transform(function ($item) {
            $key = self::replace_whitespace($item);
            return ['name'=>$item,'url'=>'api/v1/tweet/search/?city='.$key];
        });
        return response()->json(['data'=>$history->all()]);
    }

}
