<?php
/**
 * Created by PhpStorm.
 * User: abhinav
 * Date: 10/29/17
 * Time: 11:47 AM
 */

namespace App\Http\Responses;
use Illuminate\Http\JsonResponse;

class TweetResponse extends JsonResponse
{
    protected $tweets;
    protected $status;
    protected $message;

    public function __construct($status,$message,$tweets)
    {
        //parent::__construct();
        $this->status = $status;
        $this->message = $message;
        $this->tweets = $tweets;
    }

    public function toResponse()
    {
        dd($this->transformPosts());
        return response()->json($this->transformPosts());
    }

    protected function transformPosts()
    {
        $resp = [
            'status'=>$this->status,
            'message'=>$this->message,
        ];

        if($this->status = 'success'){
            $resp['data'] = $this->tweets->map(function ($tweet) {
                return [
                    'text' => $tweet->text,
                    'lat' => $tweet->lat,
                    'lng' => $tweet->lng,
                    'user_profile' => $tweet->user_profile,
                    'created_at' => $tweet->created_at->toIso8601String(),
                ];
            });
        }
dd($resp);
        return $resp;

    }
}