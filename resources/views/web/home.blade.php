<h2>Tweets from {{$city}}</h2>
<div style="height: 500px; display: flex">
    <div style="width: 80%">
        <ng-map center="[27.7172, 85.3240]" geo-fallback-center="27.7172, 85.3240" zoom-to-include-markers="true"
                style="height:500px" default-style="false">
            <marker id='{{shop.id}}' position="{{shop.position}}"
                    ng-repeat="shop in vm.tweets"
                    on-click="vm.showDetail(shop)">
            </marker>
            <info-window id="foo-iw">
                <div ng-non-bindable="">
                    id: {{vm.shop.id}}<br/>
                    name: {{vm.shop.name}}<br/>
                    Position 1: {{vm.shop.position}}<br/>
                    Position 2: {{anchor.getPosition()}}<br/>
                    <a href="#" ng-click="vm.clicked()">Click Here</a>
                </div>
            </info-window>
        </ng-map>
    </div>
    <div style="width: 20%" ng-show="vm.showHistoryList">
        <div ng-repeat="place in vm.searchHistoryList" ng-click="vm.handleHistoryListClick(place)">
            {{place.name}}
        </div>
    </div>
</div>

<div style="display: flex">
    <input type="text" placeholder="search Marker" ng-model="vm.placeName">
    <button ng-click="vm.handleSearchButtonClick()">Search</button>
    <button ng-click="vm.handleHistoryButtonClick()">View History</button>
</div>
