<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="myApp" class="no-js"> <!--<![endif]-->
<head>
	<title>Tweet Location Map</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>

<body >
	<div>
		<div ng-view>
		</div>
	</div>
	<script src="https://maps.googleapis.com/maps/api/js?key={{getenv('GOOGLE_MAP_API_KEY')}}"></script>
	<script type='text/javascript' src="{{ asset('bower_components/angular/angular.js') }}"></script>
	<script type='text/javascript' src="{{ asset('bower_components/angular-route/angular-route.min.js') }}"></script>
	<script type='text/javascript' src="{{ asset('bower_components/ng-map.min.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/app1.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/controller.js') }}"></script>



</body>

</html> 

