<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomepageTest extends TestCase
{
    /** @test index() */
    public function it_retrieves_home_screen()
    {
        $city = 'Kathmandu';
        $response = $this->json('get', '/', ['city' => $city]);

        $response->assertStatus(200)
            ->assertSeeText('Tweets from '.$city);
            //->assertViewHas('Tweets from Bangkok');
    }
}
