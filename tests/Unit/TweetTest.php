<?php

namespace Tests\Feature\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TweetTest extends TestCase
{

    /** @test index() */
    public function it_retrieves_all_tweets_according_to_city(){

        $response = $this->json('get', '/api/v1/tweet', ['city' => 'Kathmandu']);

        $response->assertStatus(200)
            //->assertJsonFragment(['status' => 'success', 'message' => 'Tweets retrieved successfully'])
            ->assertJsonStructure(['status', 'message']);
    }
}
