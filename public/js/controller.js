'use strict';

angular.module('myApp.home', ['ngRoute', 'ngMap'])

    // Route Configurations
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'js/home.html',
            controller: 'HomeCtrl',
            controllerAs: 'vm'
        });
    }])

    // service to get Tweets response from server
    .service("HomeService", ['$http', function ($http) {
        var vm = this;

        //get the markerList according to search text
        vm.getMarkerList = function (placeName) {

            return $http({
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: 'api/v1/tweet/search/?city=' + placeName
            });
        };

        //get the search history list
        vm.getSearchHistoryList = function () {
            return $http({
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: 'api/v1/tweet/history'
            });
        };
    }])

    //Controller
    .controller('HomeCtrl', ['NgMap', 'HomeService', function (NgMap, HomeService) {
        var vm = this;
        vm.map = '';
        vm.tweets = [];
        vm.tweet = '';
        vm.placeName = '';
        vm.searchHistoryList = [];
        vm.showHistoryList = false;
        vm.mapCenter = [27.7172, 85.3240];
        vm.googleMapBounds = new google.maps.LatLngBounds();
        vm.tweetsMarker = '';
        vm.tweetResponse = true;
        vm.tweetLocationName ='';

        //get the map List
        NgMap.getMap().then(function (map) {
            vm.map = map;
        });

        // Render Tweets in the map
        vm.renderMarkerToMap = function (placeName) {

            vm.tweetResponse = true;
            vm.placeName = placeName;


            vm.tweetsMarker = HomeService.getMarkerList(placeName);
            if (vm.tweetsMarker) {

                vm.tweetsMarker.then(function (response) {

                    var responseContent = response.data.data;
                    if(!responseContent.length){
                        vm.tweetResponse = false;
                        return false;
                    };


                    vm.tweets = [];
                    var googleMapBounds = new google.maps.LatLngBounds();
                    angular.forEach(responseContent, function (value, key) {
                        var latLng = {lat: value.location.lat, lng: value.location.lng};
                        googleMapBounds.extend(latLng);

                        var loc = {
                            id: key,
                            tweet: value.tweet_text,
                            user_profile: value.user.profile_picture,
                            created_at: "2017-10-30 03:02:21",
                            position: [value.location.lat, value.location.lng]
                        };
                        this.push(loc);

                    }, vm.tweets);


                    if(vm.map){

                        vm.map.setCenter(googleMapBounds.getCenter());
                        vm.map.fitBounds(googleMapBounds);
                    }

                    vm.tweetLocationName = placeName;

                });


            }
            ;
        };


        vm.mapCallBack = function () {
            //callBack function for map
        };

        //on marker click
        vm.handleMarkerClick = function (event) {
            vm.map.showInfoWindow(1, this);
        };

        vm.showDetail = function (e,stweet) {
            vm.sTweet = stweet;
           vm.map.showInfoWindow('foo', stweet.id.toString());
        };

        vm.hideDetail = function () {
            vm.map.hideInfoWindow('foo-iw');
        };

        vm.handleSearchButtonClick = function () {
            //call the api according to the search text,
            // hide the history List
            vm.showHistoryList = false;
            vm.tweets = [];
            vm.renderMarkerToMap(vm.placeName);

        };

        //handle reset button click
        vm.handleHistoryButtonClick = function () {
            //TODO call the api to get the history List

            vm.searchHistoryList = [];
            HomeService.getSearchHistoryList().then(function (response) {
                //TODO assign to search History List
                if (response.data.data) {
                    vm.searchHistoryList = response.data.data;
                }
            });

            vm.showHistoryList = true;
        };


        vm.handleHistoryListClick = function (element) {
            var placeName = element.name;
            vm.renderMarkerToMap(placeName);

        };


        vm.initializeMap = function () {
            vm.placeName = 'bangkok';
            vm.handleSearchButtonClick();

        };

        //initialize the data in the map.
        vm.initializeMap();

    }]);
